document.getElementById("generarEdades").addEventListener("click", function() {
    // Limpiar resultados anteriores
    document.getElementById("edadesGeneradas").textContent = "";
    document.getElementById("bebeTotal").textContent = 0;
    document.getElementById("niñoTotal").textContent = 0;
    document.getElementById("adolescenteTotal").textContent = 0;
    document.getElementById("adultoTotal").textContent = 0;
    document.getElementById("ancianoTotal").textContent = 0;
    document.getElementById("promedioEdad").textContent = "";

    // Generar edades aleatorias para 100 personas
    const edades = [];
    for (let i = 0; i < 100; i++) {
        edades.push(Math.floor(Math.random() * 91)); // Edades de 0 a 90 años
    }

    // Mostrar las edades generadas
    document.getElementById("edadesGeneradas").textContent = edades.join(", ");

    // Clasificar edades y calcular el promedio
    let bebe = 0, niño = 0, adolescente = 0, adulto = 0, anciano = 0, total = 0, sumaEdades = 0;
    edades.forEach(function(edad) {
        if (edad >= 0 && edad <= 2) {
            bebe++;
        } else if (edad >= 3 && edad <= 12) {
            niño++;
        } else if (edad >= 13 && edad <= 17) {
            adolescente++;
        } else if (edad >= 18 && edad <= 64) {
            adulto++;
        } else if (edad >= 65) {
            anciano++;
        }
        total++;
        sumaEdades += edad;
    });

    // Mostrar totales
    document.getElementById("bebeTotal").textContent = bebe;
    document.getElementById("niñoTotal").textContent = niño;
    document.getElementById("adolescenteTotal").textContent = adolescente;
    document.getElementById("adultoTotal").textContent = adulto;
    document.getElementById("ancianoTotal").textContent = anciano;

    // Calcular el promedio de edades
    const promedio = sumaEdades / total;
    document.getElementById("promedioEdad").textContent = promedio.toFixed(2);
});

document.getElementById("limpiarEdades").addEventListener("click", function() {
    document.getElementById("edadesGeneradas").textContent = "";
    document.getElementById("bebeTotal").textContent = 0;
    document.getElementById("niñoTotal").textContent = 0;
    document.getElementById("adolescenteTotal").textContent = 0;
    document.getElementById("adultoTotal").textContent = 0;
    document.getElementById("ancianoTotal").textContent = 0;
    document.getElementById("promedioEdad").textContent = "";
});
