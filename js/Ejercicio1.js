function calcularImporte() {
    const tipoViaje = document.getElementById("cmdViaje").value;
    const precio = parseFloat(document.getElementById("txtPrecio").value);
    const subtotal = parseFloat(document.getElementById("txtSubtotal").value);

    let importeTotal;

    if (tipoViaje === "Doble") {
        importeTotal = (precio * 0.80) + precio;
    } else {
        importeTotal = precio;
    }

    impuesto = importeTotal * 0.16;
    const total = importeTotal + impuesto;

    document.getElementById("txtSubtotal").value = importeTotal.toFixed(2);
    document.getElementById("txtImpuesto").value = impuesto.toFixed(2);
    document.getElementById("txtTotal").value = total.toFixed(2);
}

function limpiar() {
    document.getElementById("txtNumero").value = "";
    document.getElementById("txtNombreCliente").value = "";
    document.getElementById("txtDestino").value = "";
    document.getElementById("txtPrecio").value = "";
    document.getElementById("cmdViaje").value = "Sencillo";
    document.getElementById("txtSubtotal").value = "";
    document.getElementById("txtImpuesto").value = "";
    document.getElementById("txtTotal").value = "";
}
document.getElementById("Calcular").addEventListener("click", calcularImporte);
document.getElementById("Limpiar").addEventListener("click", limpiar);